//
//  FiltersCollectionViewCell.swift
//  ImageProcessor
//
//  Created by Serhii Kobzin on 3/5/18.
//  Copyright © 2018 Onix Systems. All rights reserved.
//

import UIKit

class FiltersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var filteredImageView: UIImageView!
    
    func setup(with filter: ImageFilter) {
        filter.apply(completion: { [weak self] result in
            switch result {
            case .error(let error):
                print(error.localizedDescription)
            case .result(let image):
                self?.filteredImageView.image = image
            }
        })
    }
    
}
