//
//  ImageCropper.swift
//  ImageProcessor
//
//  Created by Serhii Kobzin on 3/12/18.
//  Copyright © 2018 Onix Systems. All rights reserved.
//

import Photos
import UIKit



enum ImageCropperDataResult {
    
    case result(UIImage?)
    case error(Error)
    
}



class ImageCropper {
    
    var frameRect: CGRect?
    var imageRect: CGRect?
    var inputImage: UIImage?
    var inputImageURL: URL? {
        didSet {
            guard let inputImageURL = inputImageURL else {
                return
            }
            let fetchOptions = PHFetchOptions()
            fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
            fetchOptions.fetchLimit = 1
            let fetchResult = PHAsset.fetchAssets(withALAssetURLs: [inputImageURL], options: fetchOptions)
            guard let phAsset = fetchResult.firstObject else {
                return
            }
            self.phAsset = phAsset
        }
    }
    
    private var phAsset: PHAsset?
    private var workingImage: UIImage?
    
    func apply(completion: @escaping (ImageCropperDataResult) -> ()) {
        if inputImageURL != nil {
            getImageWithAsset(completion: { [weak self] (success) in
                if !success {
                    completion(.error(NSError(domain: "Couldn't get image with setting URL.", code: -1, userInfo: nil)))
                    return
                }
                self?.cropImage(completion: completion)
            })
        } else if inputImage != nil {
            workingImage = inputImage
            cropImage(completion: completion)
        } else {
            completion(.error(NSError(domain: "Input image source is not set.", code: -1, userInfo: nil)))
        }
    }
    
    private func getImageWithAsset(completion: @escaping (Bool) -> ()) {
        guard let phAsset = phAsset else {
            completion(false)
            return
        }
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        requestOptions.resizeMode = .none
        PHImageManager.default().requestImage(for: phAsset, targetSize: CGSize.zero, contentMode: .aspectFit, options: requestOptions, resultHandler: { [weak self] (image, _) in
            self?.workingImage = image
            completion(true)
        })
    }
    
    private func cropImage(completion: @escaping (ImageCropperDataResult) -> ()) {
        guard let image = self.workingImage, let imageRect = self.imageRect, let frameRect = self.frameRect else {
            completion(.error(NSError(domain: "Couldn't apply crop for this image.", code: -1, userInfo: nil)))
            return
        }
        let backgroundView = UIView(frame: frameRect)
        backgroundView.backgroundColor = .black
        UIGraphicsBeginImageContext(backgroundView.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            backgroundView.layer.render(in: context)
        }
        let fixedImageRect = CGRect(x: imageRect.origin.x - frameRect.origin.x, y: imageRect.origin.y - frameRect.origin.y, width: imageRect.size.width, height: imageRect.size.height)
        image.draw(in: fixedImageRect)
        let resultImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        completion(.result(resultImage))
    }
    
}
