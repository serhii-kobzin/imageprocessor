//
//  ImageFilter.swift
//  FilterProject
//
//  Created by Serhiy on 3/5/18.
//  Copyright © 2018 Serhiy. All rights reserved.
//

import Foundation
import Photos



enum ImageFilterDataResult {
    
    case result(UIImage?)
    case error(Error)
    
}



enum ImageSource {
    
    case image
    case phAsset
    
}



enum ImageFilterType: String {
    
    case None = "NoFilter"
    case Vignette = "CIVignette"
    case Instant = "CIPhotoEffectInstant"
    case Chrome = "CIPhotoEffectChrome"
    case Fage = "CIPhotoEffectFade"
    case Curve = "CILinearToSRGBToneCurve"
    case Noir = "CIPhotoEffectNoir"
    case Mono = "CIPhotoEffectMono"
    
    static let all = [None, Vignette, Instant, Chrome, Fage, Curve, Noir, Mono]
    
}



protocol ImageFilterProtocol {
    
    var filterType: ImageFilterType { get set }
    var targetSize: CGSize? { get set }
    var inputImage: UIImage? { get set }
    var inputImageURL: URL? { get set }
    
    func apply(completion: (ImageFilterDataResult) -> ())
    
}



class ImageFilter {
    
    var filterType: ImageFilterType = .None
    var targetSize: CGSize?
    var inputImage: UIImage?
    var inputImageURL: URL? {
        didSet {
            guard let inputImageURL = inputImageURL else {
                return
            }
            let fetchOptions = PHFetchOptions()
            fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
            fetchOptions.fetchLimit = 1
            let fetchResult = PHAsset.fetchAssets(withALAssetURLs: [inputImageURL], options: fetchOptions)
            guard let phAsset = fetchResult.firstObject else {
                return
            }
            self.phAsset = phAsset
        }
    }
    
    private let context = CIContext(options: nil)
    private var phAsset: PHAsset?
    private let screenScale = UIScreen.main.scale
    private var workingImage: UIImage?
    
    func apply(completion: @escaping (ImageFilterDataResult) -> ()) {
        if inputImageURL != nil {
            getImageWithAsset(completion: { [weak self] (success) in
                if !success {
                    completion(.error(NSError(domain: "Couldn't get image with setting URL.", code: -1, userInfo: nil)))
                    return
                }
                self?.applyFilter(completion: completion)
            })
        } else if inputImage != nil {
            resizeImage()
            applyFilter(completion: completion)
        } else {
            completion(.error(NSError(domain: "Input image source is not set.", code: -1, userInfo: nil)))
        }
    }
    
    private func getImageWithAsset(completion: @escaping (Bool) -> ()) {
        guard let phAsset = phAsset else {
            completion(false)
            return
        }
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        requestOptions.resizeMode = self.targetSize == nil ? .none : .exact
        var targetSize = self.targetSize ?? CGSize.zero
        targetSize = CGSize(width: targetSize.width * screenScale, height: targetSize.height * screenScale)
        PHImageManager.default().requestImage(for: phAsset, targetSize: targetSize, contentMode: .aspectFit, options: requestOptions, resultHandler: { [weak self] (image, _) in
            self?.workingImage = image
            completion(true)
        })
    }
    
    private func resizeImage() {
        guard var targetSize = self.targetSize else {
            workingImage = inputImage
            return
        }
        targetSize = CGSize(width: targetSize.width * screenScale, height: targetSize.height * screenScale)
        guard let image = inputImage else {
            return
        }
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        let newSize = widthRatio > heightRatio ? CGSize(width: image.size.width * heightRatio, height: image.size.height * heightRatio) : CGSize(width: image.size.width * widthRatio,  height: image.size.height * widthRatio)
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        workingImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
    
    private func applyFilter(completion: @escaping (ImageFilterDataResult) -> ()) {
        guard let image = workingImage else {
            completion(.error(NSError(domain: "Couldn't apply such filter to image.", code: -1, userInfo: nil)))
            return
        }
        if filterType == .None {
            completion(.result(image))
            return
        }
        guard let sourceImage = CIImage(image: image), let filter = CIFilter(name: filterType.rawValue) else {
            completion(.error(NSError(domain: "Couldn't apply such filter to image.", code: -1, userInfo: nil)))
            return
        }
        filter.setDefaults()
        if filter.inputKeys.contains(kCIInputIntensityKey) {
            filter.setValue(5.0, forKey: kCIInputIntensityKey)
        }
        filter.setValue(sourceImage, forKey: kCIInputImageKey)
        let outputCGImage = context.createCGImage((filter.outputImage)!, from: (filter.outputImage!.extent))
        let outputImage = UIImage(cgImage: outputCGImage!)
        completion(.result(outputImage))
    }
    
}
