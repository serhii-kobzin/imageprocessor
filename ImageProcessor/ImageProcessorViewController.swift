//
//  ImageProcessorViewController.swift
//  ImageProcessor
//
//  Created by Serhii Kobzin on 3/5/18.
//  Copyright © 2018 Onix Systems. All rights reserved.
//

import AVFoundation
import Photos
import UIKit

class ImageProcessorViewController: UIViewController {

    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var filtersCollectionView: UICollectionView!
    
    let imagePickerController = UIImagePickerController()
    let imageFilter = ImageFilter()
    let imageCropper = ImageCropper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePickerController.delegate = self
    }
    
    @IBAction func selectPhotoButtonDidTap(_ sender: UIButton) {
        showActionSheet()
    }
    
    func showActionSheet() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Camera", style: .default, handler: { [weak self] _ in
            self?.cameraDidSelect()
        }))
        alertController.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { [weak self] _ in
            self?.photoLibraryDidSelect()
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alertController, animated: true)
        
    }
    
    func cameraDidSelect() {
        if !UIImagePickerController.isSourceTypeAvailable(.camera) {
            print("Camera is not availableon this device")
            return
        }
        if AVCaptureDevice.authorizationStatus(for: .video) == .notDetermined {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { [weak self] (granted) in
                guard let `self` = self else {
                    return
                }
                if granted {
                    self.imagePickerController.sourceType = .camera
                    self.present(self.imagePickerController, animated: true)
                } else {
                    print("Access to camera is not granted")
                }
            })
        }
    }
    
    func photoLibraryDidSelect() {
        switch PHPhotoLibrary.authorizationStatus() {
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ [weak self] status in
                guard let `self` = self else {
                    return
                }
                if status == .authorized{
                    self.imagePickerController.sourceType = .photoLibrary
                    self.present(self.imagePickerController, animated: true)
                } else {
                    print("Access to photo library is not granted")
                }
            })
        case .authorized:
            self.imagePickerController.sourceType = .photoLibrary
            self.present(self.imagePickerController, animated: true)
        case .denied:
            print("Access to photo library is not granted")
        default:
            break
        }
    }
    
    func applyFilter(for imageWithURL: URL) {
        imageFilter.inputImageURL = imageWithURL
        imageFilter.filterType = ImageFilterType.None
        imageFilter.targetSize = mainImageView.bounds.size
        imageFilter.apply(completion: { [weak self] result in
            switch result {
            case .error(let error):
                print(error.localizedDescription)
            case .result(let image):
                self?.mainImageView.image = image
            }
        })
        filtersCollectionView.reloadData()
    }
    
    func applyFilter(for image: UIImage) {
        imageFilter.inputImage = image
        imageFilter.filterType = ImageFilterType.None
        imageFilter.targetSize = mainImageView.bounds.size
        imageFilter.apply(completion: { [weak self] result in
            switch result {
            case .error(let error):
                print(error.localizedDescription)
            case .result(let image):
                self?.mainImageView.image = image
            }
        })
        filtersCollectionView.reloadData()
    }
    
    func crop(image: UIImage) {
        let screenScale = UIScreen.main.scale
        imageCropper.inputImage = image
        imageCropper.imageRect = CGRect(x: -200.0, y: -150.0, width: image.size.width / screenScale, height: image.size.height / screenScale)
        imageCropper.frameRect = CGRect(x: 100.0, y: 300.0, width: 500.0, height: 500.0)
        imageCropper.apply(completion: { [weak self] result in
            switch result {
            case .error(let error):
                print(error.localizedDescription)
            case .result(let image):
                self?.mainImageView.image = image
            }
        })
        filtersCollectionView.reloadData()
    }
    
}

extension ImageProcessorViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//        guard let chosenImageURL = info[UIImagePickerControllerReferenceURL] as? URL else {
//            return
//        }
        guard let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            return
        }
        print(chosenImage.size)
        dismiss(animated: true, completion:  { [weak self] in
            DispatchQueue.main.async {
                self?.crop(image: chosenImage)
//                self?.applyFilter(for: chosenImage)
            }
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension ImageProcessorViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ImageFilterType.all.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let coefficient = mainImageView.bounds.width / mainImageView.bounds.height
        let itemHeight = collectionView.bounds.size.height
        let itemWidth = itemHeight * coefficient
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let index = indexPath.item
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "filtersCollectionViewCell", for: indexPath) as? FiltersCollectionViewCell else {
            return UICollectionViewCell()
        }
        imageFilter.filterType = ImageFilterType.all[index]
        imageFilter.targetSize = cell.filteredImageView.bounds.size
        cell.setup(with: imageFilter)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.item
        imageFilter.filterType = ImageFilterType.all[index]
        imageFilter.targetSize = mainImageView.bounds.size
        imageFilter.apply(completion: { [weak self] result in
            switch result {
            case .error(let error):
                print(error.localizedDescription)
            case .result(let image):
                self?.mainImageView.image = image
            }
        })
    }
    
}
